﻿/// <summary>  
/// Application AcquireRequestState  
/// </summary>  
/// <param name="sender"></param>  
/// <param name="e"></param>  
protected void Application_AcquireRequestState(object sender,
EventArgs e)
{

    string culture;
    HttpCookie cookie = Request.Cookies["UserLanguage"];
    if (cookie != null && cookie.Value != null
       && cookie.Value.Trim() != string.Empty)
        culture = cookie.Value;
    else
        culture = "en";

    //Default Language/Culture for all number, Date format  
    System.Threading.Thread.CurrentThread.CurrentCulture =
    System.Globalization.CultureInfo.CreateSpecificCulture("en");

    //Ui Culture for Localized text in the UI  
    System.Threading.Thread.CurrentThread.CurrentUICulture =
    new System.Globalization.CultureInfo(culture);
}